const moment = require('moment-timezone'); // will help us do all the dates math while considering the timezone
const util = require('./util');
const axios = require('axios');
const HOST = "https://6264ed8d-2a9f-4f22-98c8-626f6b5bcfe0.mock.pstmn.io";
const HOST_COPPEL = "https://qa-apipos.coppel.cloud";
const baseURL = 'https://api.amazonalexa.com';

module.exports = {
    getInfoUser(telefono){
        // List of actors with pictures and date of birth for a given day and month
        
        const url = HOST + "/getInfoUser";
        console.log(url); // in case you want to try the query in a web browser

        var config = {
            timeout: 6500, // timeout api call before we reach Alexa's 8 sec timeout, or set globally via axios.defaults.timeout
            headers: {'Content-Type': 'application/json'},
            data: {
                firstName: 'Fred'
              },
        };

        async function getJsonResponse(url, config){
            const res = await axios.post(url, config);
            return res.data;
        }

        return getJsonResponse(url, config).then((result) => {
            return result;
        }).catch((error) => {
            return null;
        });
    },
    getCuentas(cliente,origen, token){
        // List of actors with pictures and date of birth for a given day and month
        
        const url = HOST_COPPEL + "/appcoppel/api/v2/consultarAbono";
        console.log(url); // in case you want to try the query in a web browser

        let data = {
            "cliente" : cliente,
            "origen": origen
        };
        
        let options = {
          headers: {
            Authorization: token
          }
        }
        
        

        async function getJsonResponse(url, data, options){
            const res = await axios.post(url, data, options);
            return res.data;
        }

        return getJsonResponse(url, data, options).then((result) => {
            return result;
        }).catch((error) => {
            return null;
        });
    },
    getTarjetasCliente(cliente){
        // List of actors with pictures and date of birth for a given day and month
        
        const url = HOST + "/getTarjetas";
        console.log(url); // in case you want to try the query in a web browser

        var config = {
            timeout: 6500, // timeout api call before we reach Alexa's 8 sec timeout, or set globally via axios.defaults.timeout
            headers: {'Content-Type': 'application/json'},
            data: {
                firstName: 'Fred'
              },
        };

        async function getJsonResponse(url, config){
            const res = await axios.post(url, config);
            return res.data;
        }

        return getJsonResponse(url, config).then((result) => {
            return result;
        }).catch((error) => {
            return null;
        });
    },
    
    validarNipAutorizacion(nip){
        const url = HOST_COPPEL + "/club-proteccion/api/v1/validarNip";
        console.log(url); // in case you want to try the query in a web browser

        var data = {
            "nip" : nip
        };

        async function getJsonResponse(url, data){
            const res = await axios.post(url, data);
            return res.data;
        }

        return getJsonResponse(url, data).then((result) => {
            return result;
        }).catch((error) => {
            return null;
        });
    },
    getDataUser(apiaccessToken){

        console.log(baseURL + '/v2/accounts/~current/settings/Profile.email'); // in case you want to try the query in a web browser

        var config = {
            timeout: 6500, // timeout api call before we reach Alexa's 8 sec timeout, or set globally via axios.defaults.timeout
            headers: {
                 'Authorization': 'Bearer ' + apiaccessToken,
                'Content-Type': 'application/json'
                
            }
        };
        
        let url = baseURL + '/v2/accounts/~current/settings/Profile.email';

        async function getJsonResponse(url){
            const res = await axios.get(url, config);
            console.log("getDataUser", res.data)
            return res.data;
        }

        return getJsonResponse(url, config).then((result) => {
            return result;
        }).catch((error) => {
            return null;
        });
    },
}
