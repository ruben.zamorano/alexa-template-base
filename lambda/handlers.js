const Alexa = require('ask-sdk-core');
const util = require('./util'); // utility functions
const logic = require('./logic'); // this file encapsulates all "business" logic
const constants = require('./constants'); // constants such as specific service permissions go here

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    async handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();


        let apiaccessToken = handlerInput.requestEnvelope.context.System.apiAccessToken;
        console.log("response_dataUser",apiaccessToken);
        
        let response_dataUser =  await logic.getDataUser(apiaccessToken);
        
        console.log("response_dataUser",response_dataUser)

        // let accessToken = handlerInput.requestEnvelope.getSession().getUser().getAccessToken();
        // console.log("response_dataUser", accessToken)
        
        // const response_dataUser = await logic.getDataUser(accessToken);
        
        // console.log("response_dataUser", response_dataUser)
        
        
        
        sessionAttributes['mailUserLinker'] = response_dataUser;
       
        
        
        let name = sessionAttributes['name'] || '';

        let speechText = "hola " + name + ", bienvenido a tu tienda cóppel, aqui podras realizar tus abónos de manera rápida y sencilla. ";


        //validar cuenta registrada
        if (sessionAttributes['cuentaValidada']) {
            
            const response = await logic.getInfoUser("");
            const response_cuentas = await logic.getCuentas("");

            let lstCuentas = [];
            let totalAbono = 0;
            for (let i in response_cuentas.cuentas) {
                lstCuentas.push(response_cuentas.cuentas[i].concepto);
                totalAbono += parseInt(response_cuentas.cuentas[i].adeudo);
            }


            sessionAttributes['cuentas'] = response_cuentas.cuentas;
            sessionAttributes['totalAbono'] = totalAbono;



            speechText = speechText + "Al dia de hoy tienes " + response_cuentas.cuentas.length + " cuentas pendientes y el total es de " + totalAbono + " pesos. ";
            speechText += "Quieres proceder al pago o quieres abonar otra cantidad?"




            // const day = sessionAttributes['day'];
            // const monthName = sessionAttributes['monthName'];
            // const year = sessionAttributes['year'];
            // const name = sessionAttributes['name'] || '';
            // const sessionCounter = sessionAttributes['sessionCounter'];

            // const dateAvailable = day && monthName && year;
            // if (dateAvailable){
            //     // we can't use intent chaining because the target intent is not dialog based
            //     return SayBirthdayIntentHandler.handle(handlerInput);
            // }

            // let speechText = !sessionCounter ? handlerInput.t('WELCOME_MSG', {name: name}) : handlerInput.t('WELCOME_BACK_MSG', {name: name});
            // speechText += handlerInput.t('MISSING_MSG');

            //return AgregarCuentaIntentHandler.handle(handlerInput);

            // we use intent chaining to trigger the birthday registration multi-turn
            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt("cual es tu nip de verificacion?")
                // we use intent chaining to trigger the birthday registration multi-turn
                // .addDelegateDirective({
                //     name: 'SeleccionarCuentasIntent',
                //     confirmationStatus: 'NONE',
                //     slots: {}
                // })
                .getResponse();
        }
        else {

           
            speechText += "Para poder empezar a abonar primero tienes que iniciar sesion en alexa.coppel.com y decirme tu nip de verificacion."

            
            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt("Cual es tu nip de verificacion?")
                // .addDelegateDirective({
                //     name: 'AgregarCuentaIntent',
                //     confirmationStatus: 'NONE',
                //     slots: {}
                // })
                .getResponse();
        }







    }
};

const RegisterBirthdayIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'RegisterBirthdayIntent';
    },
    handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        if (intent.confirmationStatus === 'CONFIRMED') {
            const day = Alexa.getSlotValue(requestEnvelope, 'day');
            const year = Alexa.getSlotValue(requestEnvelope, 'year');
            // we get the slot instead of the value directly as we also want to fetch the id
            const monthSlot = Alexa.getSlot(requestEnvelope, 'month');
            const monthName = monthSlot.value;
            const month = monthSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id; //MM

            sessionAttributes['day'] = day;
            sessionAttributes['month'] = month; //MM
            sessionAttributes['monthName'] = monthName;
            sessionAttributes['year'] = year;
            // we can't use intent chaining because the target intent is not dialog based
            return SayBirthdayIntentHandler.handle(handlerInput);
        }

        return handlerInput.responseBuilder
            .speak(handlerInput.t('REJECTED_MSG'))
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};

const SayBirthdayIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SayBirthdayIntent';
    },
    async handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        const day = sessionAttributes['day'];
        const month = sessionAttributes['month']; //MM
        const year = sessionAttributes['year'];
        const name = sessionAttributes['name'] || '';
        let timezone = sessionAttributes['timezone'];

        let speechText = '', isBirthday = false;
        const dateAvailable = day && month && year;
        if (dateAvailable) {
            if (!timezone) {
                //timezone = 'Europe/Rome';  // so it works on the simulator, you should uncomment this line, replace with your time zone and comment sentence below
                return handlerInput.responseBuilder
                    .speak(handlerInput.t('NO_TIMEZONE_MSG'))
                    .getResponse();
            }
            const birthdayData = logic.getBirthdayData(day, month, year, timezone);
            sessionAttributes['age'] = birthdayData.age;
            sessionAttributes['daysLeft'] = birthdayData.daysUntilBirthday;
            speechText = handlerInput.t('DAYS_LEFT_MSG', { name: name, count: birthdayData.daysUntilBirthday });
            speechText += handlerInput.t('WILL_TURN_MSG', { count: birthdayData.age + 1 });
            isBirthday = birthdayData.daysUntilBirthday === 0;
            if (isBirthday) { // it's the user's birthday!
                speechText = handlerInput.t('GREET_MSG', { name: name });
                speechText += handlerInput.t('NOW_TURN_MSG', { count: birthdayData.age });
                const adjustedDate = logic.getAdjustedDate(timezone);
                // we'll now fetch celebrity birthdays from an external API
                const response = await logic.fetchBirthdays(adjustedDate.day, adjustedDate.month, constants.MAX_BIRTHDAYS);
                console.log(JSON.stringify(response));
                // below we convert the API response to text that Alexa can read
                const speechResponse = logic.convertBirthdaysResponse(handlerInput, response, false);
                speechText += speechResponse;
            }
            speechText += handlerInput.t('POST_SAY_HELP_MSG');

            // Add APL directive to response
            if (util.supportsAPL(handlerInput)) {
                const { Viewport } = handlerInput.requestEnvelope.context;
                const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
                handlerInput.responseBuilder.addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    version: '1.1',
                    document: constants.APL.launchDoc,
                    datasources: {
                        launchData: {
                            type: 'object',
                            properties: {
                                headerTitle: handlerInput.t('LAUNCH_HEADER_MSG'),
                                mainText: isBirthday ? sessionAttributes['age'] : handlerInput.t('DAYS_LEFT_MSG', { name: '', count: sessionAttributes['daysLeft'] }),
                                hintString: handlerInput.t('LAUNCH_HINT_MSG'),
                                logoImage: isBirthday ? null : Viewport.pixelWidth > 480 ? util.getS3PreSignedUrl('Media/full_icon_512.png') : util.getS3PreSignedUrl('Media/full_icon_108.png'),
                                backgroundImage: isBirthday ? util.getS3PreSignedUrl('Media/cake_' + resolution + '.png') : util.getS3PreSignedUrl('Media/papers_' + resolution + '.png'),
                                backgroundOpacity: isBirthday ? "1" : "0.5"
                            },
                            transformers: [{
                                inputPath: 'hintString',
                                transformer: 'textToHint',
                            }]
                        }
                    }
                });
            }

            // Add home card to response
            // If you're using an Alexa Hosted Skill the images below will expire
            // and could not be shown in the card. You should replace them with static images
            handlerInput.responseBuilder.withStandardCard(
                handlerInput.t('LAUNCH_HEADER_MSG'),
                isBirthday ? sessionAttributes['age'] : handlerInput.t('DAYS_LEFT_MSG', { name: '', count: sessionAttributes['daysLeft'] }),
                isBirthday ? util.getS3PreSignedUrl('Media/cake_480x480.png') : util.getS3PreSignedUrl('Media/papers_480x480.png'));
        } else {
            speechText += handlerInput.t('MISSING_MSG');
            // we use intent chaining to trigger the birthday registration multi-turn
            handlerInput.responseBuilder.addDelegateDirective({
                name: 'RegisterBirthdayIntent',
                confirmationStatus: 'NONE',
                slots: {}
            });
        }

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};

const RemindBirthdayIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'RemindBirthdayIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, serviceClientFactory, requestEnvelope } = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        const day = sessionAttributes['day'];
        const month = sessionAttributes['month'];
        const year = sessionAttributes['year'];
        const name = sessionAttributes['name'] || '';
        let timezone = sessionAttributes['timezone'];
        const message = Alexa.getSlotValue(requestEnvelope, 'message');

        if (intent.confirmationStatus !== 'CONFIRMED') {
            return handlerInput.responseBuilder
                .speak(handlerInput.t('CANCEL_MSG') + handlerInput.t('REPROMPT_MSG'))
                .reprompt(handlerInput.t('REPROMPT_MSG'))
                .getResponse();
        }

        let speechText = '';
        const dateAvailable = day && month && year;
        if (dateAvailable) {
            if (!timezone) {
                //timezone = 'Europe/Rome';  // so it works on the simulator, you should uncomment this line, replace with your time zone and comment sentence below
                return handlerInput.responseBuilder
                    .speak(handlerInput.t('NO_TIMEZONE_MSG'))
                    .getResponse();
            }

            const birthdayData = logic.getBirthdayData(day, month, year, timezone);
            let errorFlag = false;
            // let's create a reminder via the Reminders API
            // don't forget to enable this permission in your skill configuratiuon (Build tab -> Permissions)
            // or you'll get a SessionEnndedRequest with an ERROR of type INVALID_RESPONSE
            try {
                const { permissions } = requestEnvelope.context.System.user;
                if (!(permissions && permissions.consentToken))
                    throw { statusCode: 401, message: 'No permissions available' }; // there are zero permissions, no point in intializing the API
                const reminderServiceClient = serviceClientFactory.getReminderManagementServiceClient();
                // reminders are retained for 3 days after they 'remind' the customer before being deleted
                const remindersList = await reminderServiceClient.getReminders();
                console.log('Current reminders: ' + JSON.stringify(remindersList));
                // delete previous reminder if present
                const previousReminder = sessionAttributes['reminderId'];
                if (previousReminder) {
                    try {
                        if (remindersList.totalCount !== "0") {
                            await reminderServiceClient.deleteReminder(previousReminder);
                            delete sessionAttributes['reminderId'];
                            console.log('Deleted previous reminder token: ' + previousReminder);
                        }
                    } catch (error) {
                        // fail silently as this means the reminder does not exist or there was a problem with deletion
                        // either way, we can move on and create the new reminder
                        console.log('Failed to delete reminder: ' + previousReminder + ' via ' + JSON.stringify(error));
                    }
                }
                // create reminder structure
                const reminder = logic.createBirthdayReminder(
                    birthdayData.daysUntilBirthday,
                    timezone,
                    Alexa.getLocale(requestEnvelope),
                    message);
                const reminderResponse = await reminderServiceClient.createReminder(reminder); // the response will include an "alertToken" which you can use to refer to this reminder
                // save reminder id in session attributes
                sessionAttributes['reminderId'] = reminderResponse.alertToken;
                console.log('Reminder created with token: ' + reminderResponse.alertToken);
                speechText = handlerInput.t('REMINDER_CREATED_MSG', { name: name });
                speechText += handlerInput.t('POST_REMINDER_HELP_MSG');
            } catch (error) {
                console.log(JSON.stringify(error));
                errorFlag = true;
                switch (error.statusCode) {
                    case 401: // the user has to enable the permissions for reminders, let's attach a permissions card to the response
                        handlerInput.responseBuilder.withAskForPermissionsConsentCard(constants.REMINDERS_PERMISSION);
                        speechText = handlerInput.t('MISSING_PERMISSION_MSG');
                        break;
                    case 403: // devices such as the simulator do not support reminder management
                        speechText = handlerInput.t('UNSUPPORTED_DEVICE_MSG');
                        break;
                    //case 405: METHOD_NOT_ALLOWED, please contact the Alexa team
                    default:
                        speechText = handlerInput.t('REMINDER_ERROR_MSG');
                }
                speechText += handlerInput.t('REPROMPT_MSG');
            }

            // Add APL directive to response
            if (util.supportsAPL(handlerInput) && !errorFlag) {
                const { Viewport } = handlerInput.requestEnvelope.context;
                const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
                handlerInput.responseBuilder.addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    version: '1.1',
                    document: constants.APL.launchDoc,
                    datasources: {
                        launchData: {
                            type: 'object',
                            properties: {
                                headerTitle: handlerInput.t('LAUNCH_HEADER_MSG'),
                                mainText: handlerInput.t('REMINDER_CREATED_MSG', { name: name }),
                                hintString: handlerInput.t('LAUNCH_HINT_MSG'),
                                logoImage: Viewport.pixelWidth > 480 ? util.getS3PreSignedUrl('Media/full_icon_512.png') : util.getS3PreSignedUrl('Media/full_icon_108.png'),
                                backgroundImage: util.getS3PreSignedUrl('Media/straws_' + resolution + '.png'),
                                backgroundOpacity: "0.5"
                            },
                            transformers: [{
                                inputPath: 'hintString',
                                transformer: 'textToHint',
                            }]
                        }
                    }
                });
            }

            // Add home card to response
            // If you're using an Alexa Hosted Skill the images below will expire
            // and could not be shown in the card. You should replace them with static images
            handlerInput.responseBuilder.withStandardCard(
                handlerInput.t('LAUNCH_HEADER_MSG'),
                handlerInput.t('REMINDER_CREATED_MSG', { name: name }),
                util.getS3PreSignedUrl('Media/straws_480x480.png'));
        } else {
            speechText += handlerInput.t('MISSING_MSG');
            // we use intent chaining to trigger the birthday registration multi-turn
            handlerInput.responseBuilder.addDelegateDirective({
                name: 'RegisterBirthdayIntent',
                confirmationStatus: 'NONE',
                slots: {}
            });
        }

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};

const CelebrityBirthdaysIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'CelebrityBirthdaysIntent';
    },
    async handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes()
        const name = sessionAttributes['name'] || '';
        let timezone = sessionAttributes['timezone'];

        if (!timezone) {
            //timezone = 'Europe/Rome';  // so it works on the simulator, you should uncomment this line, replace with your time zone and comment sentence below
            return handlerInput.responseBuilder
                .speak(handlerInput.t('NO_TIMEZONE_MSG'))
                .getResponse();
        }
        try {
            // call the progressive response service
            await util.callDirectiveService(handlerInput, handlerInput.t('PROGRESSIVE_MSG', { name: name }));
        } catch (error) {
            // if it fails we can continue, but the user will wait without progressive response
            console.log("Progressive response directive error : " + error);
        }
        const adjustedDate = logic.getAdjustedDate(timezone);
        // we'll now fetch celebrity birthdays from an external API
        const response = await logic.fetchBirthdays(adjustedDate.day, adjustedDate.month, constants.MAX_BIRTHDAYS);
        console.log(JSON.stringify(response));
        // below we convert the API response to text that Alexa can read
        const speechResponse = logic.convertBirthdaysResponse(handlerInput, response, true, timezone);
        let speechText = handlerInput.t('API_ERROR_MSG');
        if (speechResponse) {
            speechText = speechResponse;
        }
        speechText += handlerInput.t('POST_CELEBRITIES_HELP_MSG');

        // Add APL directive to response
        if (util.supportsAPL(handlerInput) && speechResponse) { // empty speechResponse -> no API results
            const { Viewport } = handlerInput.requestEnvelope.context;
            const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.1',
                document: constants.APL.launchDoc,
                datasources: {
                    launchData: {
                        type: 'object',
                        properties: {
                            headerTitle: handlerInput.t('LAUNCH_HEADER_MSG'),
                            mainText: logic.convertBirthdaysResponse(handlerInput, response, false).split(": ")[1],
                            hintString: handlerInput.t('LAUNCH_HINT_MSG'),
                            logoImage: Viewport.pixelWidth > 480 ? util.getS3PreSignedUrl('Media/full_icon_512.png') : util.getS3PreSignedUrl('Media/full_icon_108.png'),
                            backgroundImage: util.getS3PreSignedUrl('Media/lights_' + resolution + '.png'),
                            backgroundOpacity: "0.5"
                        },
                        transformers: [{
                            inputPath: 'hintString',
                            transformer: 'textToHint',
                        }]
                    }
                }
            });

            // Add home card to response
            // If you're using an Alexa Hosted Skill the images below will expire
            // and could not be shown in the card. You should replace them with static images
            handlerInput.responseBuilder.withStandardCard(
                handlerInput.t('LAUNCH_HEADER_MSG'),
                speechResponse,
                util.getS3PreSignedUrl('Media/lights_480x480.png'));
        }

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = handlerInput.t('HELP_MSG');

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        const name = sessionAttributes['name'] || '';
        const speechText = handlerInput.t('GOODBYE_MSG', { name: name });

        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(true) // session can remain open if APL doc was rendered
            .getResponse();
    }
};




const RegistrarTelefonoIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'RegistrarTelefonoIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        const telefono = Alexa.getSlotValue(requestEnvelope, 'telefono');
        // sessionAttributes['day'] = day;


        if (intent.confirmationStatus === 'CONFIRMED') {

            // const speechText = handlerInput.t('REJECTED_MSG');
            const speechText = "tu telefono es " + telefono.value + ". quieres continuar o cambiar de numero?"

            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt(handlerInput.t('REPROMPT_MSG'))
                .getResponse();

        }


    }
};

const RegistrarNumeroClienteIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'RegistrarNumeroClienteIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        const telefono = Alexa.getSlotValue(requestEnvelope, 'telefono');
        // sessionAttributes['day'] = day;
        let telefono_format = "";
        let cont_space = 0;


        for (let i in telefono.value.toString()) {
            telefono_format += telefono.value.toString()[i];
            cont_space += 1;
            if (cont_space === 2) {
                telefono_format += " ";
                cont_space = 0;
            }

        }

        const speechText = "tu telefono es " + telefono_format.trim() + ". quieres continuar o cambiar de numero?"

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};

const AgregarCuentaIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AgregarCuentaIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        const tipoCuentaSlot = Alexa.getSlot(requestEnvelope, 'tipoCuenta');
        const tipoCuentaName = tipoCuentaSlot.value;
        const tipoCuenta = tipoCuentaSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id; //MM

        console.log("//////////////////////////////////////////////")
        console.log(tipoCuenta);
        console.log("//////////////////////////////////////////////")


        //return SayBirthdayIntentHandler.handle(handlerInput);


        if (intent.confirmationStatus === 'CONFIRMED') {
            if (tipoCuenta === "1") {


                //return SayBirthdayIntentHandler.handle(handlerInput);
                // return RegistrarTelefonoIntentHandler.handle(handlerInput);
                // return RegistrarTelefonoIntentHandler.handle(handlerInput);
                const speechText = "recuerda que tu numero de telefono debe de ser de diez digitos"
                return handlerInput.responseBuilder
                    .speak(speechText)
                    // .reprompt(speechText)
                    // we use intent chaining to trigger the birthday registration multi-turn
                    //   .addDelegateDirective({
                    //         name: 'RegistrarTelefonoIntent',
                    //         confirmationStatus: 'NONE',
                    //         slots: {}
                    //     })
                    .addDirective({
                        type: 'Dialog.Delegate',
                        updatedIntent: {
                            name: 'RegistrarTelefonoIntent',
                            confirmationStatus: 'NONE',
                            slots: {}
                        }
                    })
                    .getResponse();
            }
            else {
                //return SayBirthdayIntentHandler.handle(handlerInput);
                //  return RegistrarNumeroClienteIntentHandler.handle(handlerInput);
                const speechText = "cual es tu numero de cliente? si no lo recuerdas lo puedes ver en tu app cóppel"
                return handlerInput.responseBuilder
                    .speak(speechText)
                    .reprompt(speechText)
                    // we use intent chaining to trigger the birthday registration multi-turn
                    // .addDirective({
                    //     type: 'Dialog.Delegate',
                    //     updatedIntent: {
                    //       name: 'RegistrarNumeroClienteIntent',
                    //       confirmationStatus: 'NONE',
                    //       slots: {}
                    //     }
                    //   })
                    .getResponse();
            }

        }




        //  return handlerInput.responseBuilder
        //     .speak("elegiste ")
        //     .reprompt(handlerInput.t('REPROMPT_MSG'))
        //     .getResponse();





        // return handlerInput.responseBuilder
        //     .speak("Cual es tu tipo de ceunta?")
        //     .reprompt(handlerInput.t('REPROMPT_MSG'))
        //     .getResponse();
    }
};

const SeleccionarCuentasIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SeleccionarCuentasIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        const response_tarjetas = await logic.getTarjetasCliente("");

        // const tipoCuentaAbonoSlot = Alexa.getSlot(requestEnvelope, 'tipoCuentaAbono');
        // const tipoCuentaAbonoName = tipoCuentaAbonoSlot.value;
        // const tipoCuentaAbono = tipoCuentaAbonoSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id;

        let cuentas = sessionAttributes['cuentas'];
        let totalAbono = sessionAttributes['totalAbono']

        //let speechText = "Con que tarjeta quieres pagar las " + cuentas.length + " cuentas? el total es de " + totalAbono + " pesos.";

        let speechText = "ruben "

        if (intent.confirmationStatus === 'CONFIRMED') {

            let tarjetas = [];
            for (let i in response_tarjetas.tarjetas) {
                tarjetas.push(response_tarjetas.tarjetas[i].numero);
            }


            sessionAttributes['tarjetasCliente'] = tarjetas;
            if (response_tarjetas.tarjetas.length === 1) {
                speechText += "tienes la tarjeta con terminacion " + tarjetas[0] + " quieres continuar con el pago?"
            }
            else {
                // console.log(id.substr(id.length - 2));
                // console.log(id.substr(0, 2));
                let tarjeta1 = tarjetas[0].toString();
                tarjeta1 = tarjeta1.substr(0, 2) + " " + tarjeta1.substr(tarjeta1.length - 2);

                let tarjeta2 = tarjetas[1].toString();
                tarjeta2 = tarjeta2.substr(0, 2) + " " + tarjeta2.substr(tarjeta2.length - 2);

                speechText += "tienes la tarjeta con terminacion " + tarjeta1 + " y la tarjeta con terminacion " + tarjeta2;
                //speechText += ", con cual prefieres hacer el pago?";
            }

            //pagar todas las cuentas
            // if (tipoCuentaAbono === "0") {
            //     speechText = "Se abonaran " + cuentas.length + " cuentas con un total de " + totalAbono + " pesos, "
            //       //validar el numero de tarjetas

            //     let tarjetas = [];
            //     for(let i in response_tarjetas.tarjetas)
            //     {
            //         tarjetas.push(response_tarjetas.tarjetas[i].numero);
            //     }


            //     sessionAttributes['tarjetasCliente'] = tarjetas;
            //     if(response_tarjetas.tarjetas.length === 1)
            //     {
            //          speechText += "tienes la tarjeta con terminacion " + tarjetas[0] + " quieres continuar con el pago?"
            //     }
            //     else
            //     {
            //         // console.log(id.substr(id.length - 2));
            //         // console.log(id.substr(0, 2));
            //         let tarjeta1 = tarjetas[0].toString();
            //         tarjeta1 = tarjeta1.substr(0, 2)  + " " + tarjeta1.substr(tarjeta1.length - 2);

            //         let tarjeta2 = tarjetas[1].toString();
            //         tarjeta2 = tarjeta2.substr(0, 2) + " " + tarjeta2.substr(tarjeta2.length - 2);

            //         speechText += "tienes la tarjeta con terminacion " + tarjeta1 + " y la tarjeta con terminacion " + tarjeta2;
            //         speechText += ", con cual prefieres hacer el pago?";
            //     }



            // }
            // else {
            //     //Listar todas las ceuntas
            //     speechText = "Se abonara solo a " + tipoCuentaAbonoName;
            //     //obtener el listado de cuentas con detalle
            // }








        }








        return handlerInput.responseBuilder
            .speak(speechText)
            // .reprompt(speechText)
            // we use intent chaining to trigger the birthday registration multi-turn
            .addDelegateDirective({
                name: 'SeleccionarTarjetaIntent',
                confirmationStatus: 'NONE',
                slots: {}
            })
            // .addDirective({
            //     type: 'Dialog.Delegate',
            //     updatedIntent: {
            //       name: 'SeleccionarTarjetaIntent',
            //       confirmationStatus: 'NONE',
            //       slots: {}
            //     }
            //   })
            .getResponse();

    }
};

const SeleccionarTarjetaIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SeleccionarTarjetaIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;
        let speechText = "";

        //   let tarjetasCliente = sessionAttributes['tarjetasCliente'];

        const name = sessionAttributes['name'] || '';

        //validar la tarjeta seleccionada
        const tarjetaAbonoSlot = Alexa.getSlot(requestEnvelope, 'tarjeta');
        let tarjetaAbono = "";

        if (tarjetaAbonoSlot.value === undefined) {
            const num_unoSlot = Alexa.getSlot(requestEnvelope, 'num_uno');
            const num_uno = num_unoSlot.value;

            const num_dosSlot = Alexa.getSlot(requestEnvelope, 'num_dos');
            const num_dos = num_dosSlot.value;

            tarjetaAbono = num_uno + " " + num_dos;
        }
        else {
            tarjetaAbono = tarjetaAbonoSlot.value;
        }








        //guardar abono

        speechText = "Se generó el abono correctamente con tu tarjeta " + tarjetaAbono + ", puedes revisar tu estado de cuenta en coppel.com o en la app coppel. Gracias " + name + ", nos vemos en tu proximo abono."


        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(true)
            // .reprompt(speechText)
            // we use intent chaining to trigger the birthday registration multi-turn
            //   .addDelegateDirective({
            //         name: 'RegistrarTelefonoIntent',
            //         confirmationStatus: 'NONE',
            //         slots: {}
            //     })
            // .addDirective({
            //     type: 'Dialog.Delegate',
            //     updatedIntent: {
            //       name: 'RegistrarTelefonoIntent',
            //       confirmationStatus: 'NONE',
            //       slots: {}
            //     }
            //   })
            .getResponse();

    }
};


const SeleccionarTelefonoIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SeleccionarTelefonoIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;

        //set variable para buscar por telefono
        let speechText = "cual es tu numero de telefono?. recuerda que tu numero de telefono debe de ser de diez digitos";


        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();





    }
};

const validarNipAlexaIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'validarNipAlexaIntent';
    },
    async handle(handlerInput) {
        const { attributesManager, requestEnvelope } = handlerInput;
        // the attributes manager allows us to access session attributes
        const sessionAttributes = attributesManager.getSessionAttributes();
        const { intent } = requestEnvelope.request;
        let speechText = "";
        
        
        const nipSlot = Alexa.getSlot(requestEnvelope, 'nip');
        const nip = nipSlot.value;
        
        
        if (intent.confirmationStatus === 'CONFIRMED') {
            
            const response_validarNip = await logic.validarNipAutorizacion(nip);
            //speechText += "tu email es: " + response_validarNip.data.response.dataLogin.email;
            
            if( response_validarNip.data.response.errorCode)
            {
            
                //error
                speechText += "Al parecer este nip no es valido, quieres decirmelo otra vez?";
            }
            else
            {
                //mostrar cuentas
                sessionAttributes['dataLogin'] = response_validarNip.data.response.dataLogin;
                sessionAttributes['dataConsultarAbono'] = response_validarNip.data.response.dataConsultarAbono;
                sessionAttributes['dataAbono'] = response_validarNip.data.response.dataAbono;
                
                let totalAbono = 0;
                
                for(let i in response_validarNip.data.response.dataAbono.abonos)
                {
                    totalAbono += response_validarNip.data.response.dataAbono.abonos[i].suPago;
                }
                
                
                 const name = sessionAttributes['name'] || '';

                speechText += "hola " + name + ", ";
                if(response_validarNip.data.response.dataAbono.abonos.length === 1)
                {
                     speechText += "al dia de hoy tienes una cuenta pendiente y el total es de " + totalAbono + " pesos. ";
                }
                else
                {
                     speechText += "al dia de hoy tienes " + response_validarNip.data.response.dataAbono.abonos.length + " cuentas pendientes y el total es de " + totalAbono + " pesos. ";
                }
               
                speechText += "Quieres proceder al pago o quieres abonar otra cantidad?"
                
            }
            
                
                
    
            //set variable para buscar por telefono
            //speechText += "cual es tu numero de telefono?. recuerda que tu numero de telefono debe de ser de diez digitos";
    
    
            return handlerInput.responseBuilder
                .speak(speechText)
                .getResponse();
        }
        
       





    }
};

/* *
 * FallbackIntent triggers when a customer says something that doesn’t map to any intents in your skill
 * It must also be defined in the language model (if the locale supports it)
 * This handler can be safely added but will be ingnored in locales that do not support it yet 
 * */
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const speechText = handlerInput.t('FALLBACK_MSG');

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};
/* *
 * SessionEndedRequest notifies that a session was ended. This handler will be triggered when a currently open 
 * session is closed for one of the following reasons: 1) The user says "exit" or "quit". 2) The user does not 
 * respond or says something that does not match an intent defined in your voice model. 3) An error occurs 
 * */
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`~~~~ Session ended: ${JSON.stringify(handlerInput.requestEnvelope)}`);
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse(); // notice we send an empty response
    }
};
/* *
 * The intent reflector is used for interaction model testing and debugging.
 * It will simply repeat the intent the user said. You can create custom handlers for your intents 
 * by defining them above, then also adding them to the request handler chain below 
 * */
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speechText = handlerInput.t('REFLECTOR_MSG', { intent: intentName });

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};
/**
 * Generic error handling to capture any syntax or routing errors. If you receive an error
 * stating the request handler chain is not found, you have not implemented a handler for
 * the intent being invoked or included it in the skill builder below 
 * */
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const speechText = handlerInput.t('ERROR_MSG');
        console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(handlerInput.t('REPROMPT_MSG'))
            .getResponse();
    }
};

module.exports = {
    LaunchRequestHandler,
    SayBirthdayIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    FallbackIntentHandler,
    SessionEndedRequestHandler,
    //IntentReflectorHandler,
    SeleccionarCuentasIntentHandler,
    ErrorHandler,
    SeleccionarTarjetaIntentHandler,
    validarNipAlexaIntentHandler
}
